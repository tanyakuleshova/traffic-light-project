<?php

namespace App\Persistance\Repository;


use App\Domain\Models\Program;
use App\Domain\Repository\ProgramRepositoryInterface;

class ProgramRepository implements ProgramRepositoryInterface
{
    public function getAll()
    {
        return Program::all();
    }

    public function getById($id)
    {
        return Program::find($id);
    }

    public function getByField($field)
    {
        return Program::where($field[0], $field[1])->get();
    }

    public function delete($id)
    {
        return Program::where('id', $id)->delete();
    }
}
