<?php

namespace App\Persistance\Repository;

use App\Domain\Models\DeltaSO;
use App\Domain\Repository\DeltaSORepositoryInterface;

class DeltaSORepository implements DeltaSORepositoryInterface
{
    public function getAll()
    {
        return DeltaSO::with('so', 'scen')->get();
    }

    public function getById($id)
    {
        return DeltaSO::where('id', $id)->with('so', 'scen')->first();
    }

    public function delete($id)
    {
        return DeltaSO::where('id', $id)->delete();
    }

    public function deleteIn($ids)
    {
        return DeltaSO::whereIn('id', $ids)->delete();
    }
}
