<?php

namespace App\Persistance\Repository;

use App\Domain\Models\SO;
use App\Domain\Repository\SORepositoryInterface;

class SORepository implements SORepositoryInterface
{
    public function getAll()
    {
        return SO::with('dk')->get();
    }

    public function getById($id)
    {
        return SO::where('id', $id)->with('directions', 'dk.scen', 'conflict', 'cyclogram', 'delta')->first();
    }

    public function getByDK($id)
    {
        return SO::where('dk_id', $id)->get();
    }

    public function delete($id)
    {
        return SO::where('id', $id)->delete();
    }

}
