<?php

namespace App\Persistance\Repository;

use App\Domain\Models\InfoSO;
use App\Domain\Repository\InfoSORepositoryInterface;

class InfoSORepository implements InfoSORepositoryInterface
{
    public function getAll()
    {
        return InfoSO::with('param')->get();
    }

    public function getById($id)
    {
        return InfoSO::where('id', $id)->with('dk')->first();
    }

    public function getByParams($params)
    {
        return InfoSO::where($params)->orderBy('id', 'desc')->get();
    }

    public function delete($id)
    {
        return InfoSO::where('id', $id)->delete();
    }
}
