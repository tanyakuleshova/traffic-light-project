<?php

namespace App\Persistance\Repository;

use App\Domain\Models\Conflict;
use App\Domain\Repository\ConflictRepositoryInterface;

class ConflictRepository implements ConflictRepositoryInterface
{
    public function getAll()
    {
        return Conflict::all();
    }

    public function getById($id)
    {
        return Conflict::find($id);
    }

    public function delete($id)
    {
        return Conflict::where('id', $id)->delete();
    }

    public function getByField($field)
    {
        return Conflict::where($field[0], $field[1])->get();
    }
}
