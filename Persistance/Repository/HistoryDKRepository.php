<?php

namespace App\Persistance\Repository;

use App\Domain\Models\HistoryDK;
use App\Domain\Repository\HistoryDKRepositoryInterface;
use App\Services\Pagination\Pagination;

class HistoryDKRepository implements HistoryDKRepositoryInterface
{
    public function getAll()
    {
        return HistoryDK::with('param')->get();
    }

    public function getById($id)
    {
        return HistoryDK::where('id', $id)->with('dk')->first();
    }

    public function getByParams($params, $limit="")
    {
        if(!$limit){
            $data = HistoryDK::where($params)->orderBy('id', 'desc')->with('dk');
        }else{
            $data =  HistoryDK::where($params)->orderBy('id', 'desc')->limit($limit)->with('dk');
        }

        return count($data->get()) ? Pagination::generate($data, 20, "", "history_page") : [];

    }

    public function getByParamsLast($params, $limit="")
    {
        return HistoryDK::where($params)->orderBy('id', 'desc')->limit($limit)->with('dk')->get();
    }

    public function delete($id)
    {
        return HistoryDK::where('id', $id)->delete();
    }
}
