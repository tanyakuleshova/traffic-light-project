<?php

namespace App\Persistance\Repository;

use App\Domain\Models\DK;
use App\Domain\Repository\DKRepositoryInterface;

class DKRepository implements DKRepositoryInterface
{
    public function getAll()
    {
        return DK::with('param')->get();
    }

    public function getById($id)
    {
        return DK::where('id', $id)->with('param', 'scen', 'so.directions', 'so.delta', 'scen', 'current')->first();
    }

    public function getByNumber($part): array
    {
        $result =[];
        $items = Dk::where('number', 'like', "$part%")
            ->get();
        foreach ($items as $item){
            $nums = '';
            $pcu = '';
            foreach ($item->so as $n){
                $nums .= $n->number .',';
                $pcu .= $n->pcu .',';
            }
            $nums = rtrim($nums, ",");
            $pcu = rtrim($pcu, ",");
            $result[$item->id]= [$item->number , $nums, $pcu];
        }

        return $result;
    }

    public function delete($id)
    {
        return DK::where('id', $id)->delete();
    }
}
