<?php

namespace App\Persistance\Repository;

use App\Domain\Models\Directions;
use App\Domain\Repository\DirectionsRepositoryInterface;

class DirectionsRepository implements DirectionsRepositoryInterface
{
    public function getAll()
    {
        return Directions::all();
    }

    public function getById($id)
    {
        return Directions::find($id);
    }

    public function delete($id)
    {
        return Directions::where('id', $id)->delete();
    }
}
