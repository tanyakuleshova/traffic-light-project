<?php

namespace App\Persistance\Repository;

use App\Domain\Models\Current;
use App\Domain\Repository\CurrentRepositoryInterface;

class CurrentRepository implements CurrentRepositoryInterface
{
    public function getAll()
    {
        return Current::with('param')->get();
    }

    public function getById($id)
    {
        return Current::where('id', $id)->with('dk')->first();
    }

    public function getByParams($params)
    {
        return Current::where($params)->orderBy('id', 'desc')->get();
    }

    public function delete($id)
    {
        return Current::where('id', $id)->delete();
    }
}
