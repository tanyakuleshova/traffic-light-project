<?php

namespace App\Persistance\Repository;

use App\Domain\Models\ParamDK;
use App\Domain\Repository\ParamDKRepositoryInterface;

class ParamDKRepository implements ParamDKRepositoryInterface
{
    public function getAll()
    {
        return ParamDK::all();
    }

    public function getById($id)
    {
        return ParamDK::find($id);
    }

    public function delete($id)
    {
        return ParamDK::where('id', $id)->delete();
    }

}
