<?php

namespace App\Persistance\Repository;

use App\Domain\Models\StateDK;
use App\Domain\Repository\StateDKRepositoryInterface;

class StateDKRepository implements StateDKRepositoryInterface
{
    public function getAll()
    {
        return StateDK::with('param')->get();
    }

    public function getById($id)
    {
        return StateDK::where('id', $id)->with('dk')->first();
    }

    public function getByParams($params)
    {
        return StateDK::where($params)->orderBy('id', 'desc')->with('dk')->get();
    }

    public function delete($id)
    {
        return StateDK::where('id', $id)->delete();
    }
}
