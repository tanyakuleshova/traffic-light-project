<?php

namespace App\Persistance\Repository;

use App\Domain\Models\Cyclogram;
use App\Domain\Repository\CyclogramRepositoryInterface;

class CyclogramRepository implements CyclogramRepositoryInterface
{
    public function getAll()
    {
        return Cyclogram::all();
    }

    public function getById($id)
    {
        return Cyclogram::find($id);
    }

    public function delete($id)
    {
        return Cyclogram::where('id', $id)->delete();
    }

    public function getByField($field)
    {
        return Cyclogram::where($field[0], $field[1])->get();
    }
}
