<?php

namespace App\Persistance\Repository;

use App\Domain\Models\ScenDK;
use App\Domain\Repository\ScenDKRepositoryInterface;

class ScenDKRepository implements ScenDKRepositoryInterface
{
    public function getAll()
    {
        return ScenDK::all();
    }

    public function getById($id)
    {
        return ScenDK::find($id);
    }

    public function getByField($field)
    {
        return ScenDK::where($field[0], $field[1])->orderBy('sort')->get();
    }

    public function delete($id)
    {
        return ScenDK::where('id', $id)->delete();
    }
}
