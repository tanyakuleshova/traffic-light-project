<?php

namespace App\Services\Pagination;
use Illuminate\Pagination\LengthAwarePaginator;

class Pagination
{
    public static function generate($query, $perPage = null, $path = null, $page_name = null)
    {
        $columns = ['*'];
        $pageName = $page_name ?? 'page';
        $page = null;

        $page = $page ?: LengthAwarePaginator::resolveCurrentPage($pageName);
        $total = count(clone ($query)->get());
        $results = $total ? clone ($query)
            ->forPage($page, $perPage)->get($columns) : new \stdClass();
        $r_path = LengthAwarePaginator::resolveCurrentPath();
        return new LengthAwarePaginator($results, $total, $perPage, $page, [
            'path' => $path ? $r_path . $path : $r_path,
            'pageName' => $pageName,
        ]);
    }
}
