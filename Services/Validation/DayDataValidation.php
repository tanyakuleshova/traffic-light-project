<?php

namespace App\Services\Validation;

class DayDataValidation extends Validation
{
    function rules(): array
    {
        return [
            'day' => 'date',
            'calories' => 'numeric|max:10000|nullable',
            'weight' => 'numeric|max:1000|nullable',
            'waist' => 'numeric|max:1000|nullable',
            'breast' => 'numeric|max:1000|nullable',
            'hips' => 'numeric|max:1000|nullable',
        ];
    }
}