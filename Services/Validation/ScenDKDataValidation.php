<?php

namespace App\Services\Validation;

class ScenDKDataValidation extends Validation
{
    function rules(): array
    {
        return [
            'name' => 'required|max:200',
            'circle_time' => 'required|numeric|max:1000',
            'max_circle_time' => 'required|numeric|max:1000',
            'sort' => 'required|max:1000'
        ];
    }
}
