<?php

namespace App\Services\Validation;

class SODataValidation extends Validation
{
    function rules(): array
    {
        return [
            'address' => 'required',
            'number' => 'required|max:200',
            'name' => 'max:200',
            'dk_id' => 'required|max:200',
            'pcu' => 'nullable|max:100',
        ];
    }
}
