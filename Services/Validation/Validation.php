<?php

namespace App\Services\Validation;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

abstract class Validation
{
    public function prepare($data_request) : array
    {
        $data = [];

        foreach ($data_request as $item){
            $data[$item['name']] = $item['value'];
        }

        $errors = $this->validate($data);

        return ['data' => $data, 'errors' => $errors];
    }

    function validate($data) : array
    {
        $errors = [];
        $validator = Validator::make($data, $this->rules());

        try{
            $validator->validate();
        }catch (ValidationException $e){
            $errors = $e->errors();
        }

        return $errors;

    }

    abstract function rules() : array;

}