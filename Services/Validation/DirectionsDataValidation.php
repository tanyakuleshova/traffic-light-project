<?php

namespace App\Services\Validation;

class DirectionsDataValidation extends Validation
{
    function rules(): array
    {
        return [
            'number' => 'required|max:200',
            'name' => 'required|max:200',
            'keys' => 'required|max:100',
            'type' => 'required|max:100|numeric',

            'emitters_qty' => 'nullable|max:255',
            'limit' => 'nullable|max:255',
            'load' => 'nullable|max:255',
            'deviation' => 'nullable|max:1000',
        ];
    }
}
