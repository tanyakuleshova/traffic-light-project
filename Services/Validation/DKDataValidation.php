<?php

namespace App\Services\Validation;

class DKDataValidation extends Validation
{
    function rules(): array
    {
        return [
            'address' => 'required',
            'number' => 'required|max:200',
            'name' => 'max:200'
        ];
    }
}
