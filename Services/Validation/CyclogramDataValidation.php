<?php

namespace App\Services\Validation;

class CyclogramDataValidation extends Validation
{
    function rules(): array
    {
        return [
            'phase' => 'required|numeric|max:10'
        ];
    }
}
