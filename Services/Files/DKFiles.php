<?php

namespace App\Services\Files;

use Carbon\Carbon;

class DKFiles
{

    public $key_str = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];


    public function generate($dk)
    {
        $errors = false;
        if (!$dk->so->isEmpty()) {

            $so_s = $dk->so->sortBy('number');
            $first_so = $so_s->first();

            $scens = $dk->scen->sortBy('sort');

            $scn = 0;
            if (!$scens->isEmpty()) {
                [$rows, $errors] = $this->scn_data($so_s, $scens);
                if (empty($errors) && !empty($rows)) {
                    $scn = $this->write_scn($rows, $dk->number);
                }
            }

            $programs = $first_so->program;

            $prg = 0;
            if ($programs) {
                $rows = $this->prg_data($programs);
                if (!empty($rows)) {
                    $prg = $this->write_prg($rows, $dk->number);
                }
            }

            $key_rows = $this->key_data($so_s);

            $key = 0;
            if (!empty($key_rows)) {
                $key = $this->write_key($key_rows, $dk->number);
            }

            $param = $dk->param ? $this->write_main_cfg($dk->param) : 0;

            $md5 = 0;
            if ($prg && $scn && $key && $param) {
                $md5 = $this->create_md5($dk->number);

            }

            return response(['prg' => (bool)$prg, 'scn' => (bool)$scn, 'key' => (bool)$key, 'param' => (bool)$param, 'md5' => (bool)$md5, 'errors' => $errors, 'so' => $first_so]);
        } else {
            return response(['so' => false]);
        }

    }

    public function key_data($so_s)
    {
        $pcus = [];
        //заходим в каждое со
        foreach ($so_s->sortBy('pcu') as $so) {

            $pcu = str_replace(' ', '', $so->pcu);
            $pcu = explode(',', $pcu);

            $needed_pcus = [];

            foreach ($pcu as $comp) {
                $pcu_one = [];
                $pcu_one[] = $comp;
                $pcu_one[] = $this->key_str;
                $needed_pcus[] = $pcu_one;
            }
            //заходим в направления со
            foreach ($so->directions->sortBy('number') as $direction) {

                $controls = explode(',', $direction->control);
                $limits = explode(',', $direction->limit);
                $keys = explode(',', $direction->keys);

                //перебираем ключи со и распределяем их по pcu
                foreach ($keys as $key_index => $key_value) {

                    if ($controls[$key_index] == '1' && $limits[$key_index] !== '') {

                        $define_pcu = ceil($key_value / 8);
                        $define_pcu = (int)$define_pcu;

                        $pcu_key_position = 0;
                        for ($i = 1; $i < $key_value; $i++) {

                            $pcu_key_position++;

                            if ($pcu_key_position == 8) {
                                $pcu_key_position = 0;
                            }

                        }

                        $needed_pcus[$define_pcu - 1][1][$pcu_key_position] = $limits[$key_index];
                    }
                }
            }

            $pcus[] = $needed_pcus;

        }

        return $pcus;
    }

    public function scn_data($so_s, $scens)
    {
        $rows = [];
        $errors = [];
        foreach ($so_s->sortBy('number') as $so) {

            $pcu_qty = count(explode(',', $so->pcu));

            $so_row = [];
            //1,2,3 пункт
            $so_row[] = $so->number;
            $so_row[] = str_replace(' ', '', $so->pcu);
            $so_row[] = '0:0:0';

            //4 пункт
            $scen = $scens->sortBy('sort')->first();
            $cyclo = '';
            foreach ($so->cyclogram as $cyc) {
                if ($cyc->scen_id === $scen->id) {
                    $cyclo = $cyc;
                }
            }

            if ($cyclo !== '' && $cyclo->data !== null) {
                $data_cyc = json_decode($cyclo->data);

                //на каждой фазе заходим в направления so
                $str = '';
                for ($i = 1; $i <= $cyclo->phase; $i++) {
                    //ключи Тосн первой фазы
                    $t_osn = '';
                    foreach ($so->directions->sortBy('number') as $direction) {
                        $dir_id = $direction->id;
                        $dir_num = $direction->number;

                        if (isset($data_cyc->$dir_id->ph->$i->color) && isset($data_cyc->$dir_id->ph->$i->time)) {

                            $color = $data_cyc->$dir_id->ph->$i->color;
                            $key = $this->define_key($color, $direction);

                        } elseif (!isset($data_cyc->$dir_id->ph->$i->color) && $direction->type === 3) {

                            $key = '0';
                        } else {
                            $errors[] = "СО № $so->number, напрямок № $dir_num, фаза № $i: не встановлено колір.";
                        }
                        $t_osn .= $key ?? '';
                    }
                    $zero = count(explode(',', $so->pcu)) * 16 - strlen($t_osn);
                    for ($x = 0; $x < $zero; $x++) {
                        $t_osn .= '0';
                    }

                    $t_osn = str_pad((base_convert($t_osn, 2, 16)), 4 * $pcu_qty, '0', STR_PAD_LEFT);

                    $t_osn .= ';';
                    $str .= $t_osn;

                    //ключи Тпром первой фазы с периодом полсекунды : ключи Тосн второй фазы ...

                    $dir_ = $so->directions->sortBy('number')->first();
                    $dir = $dir_->id;
                    $dir_num = $dir_->number;

                    if (isset($data_cyc->$dir->t->$i->data) && count($data_cyc->$dir->t->$i->data) > 0) {
                        $time = count($data_cyc->$dir->t->$i->data);

                        //заходим в каждое деление t пром
                        $t_prom_keys = '';
                        for ($n = 0; $n < $time; $n++) {
                            $division = '';
                            //собираем ключи для текущего деления
                            foreach ($so->directions->sortBy('number') as $direction) {

                                $tpr = '';
                                $dir_id = $direction->id;
                                if (isset($data_cyc->$dir_id)) {
                                    foreach ($data_cyc->$dir_id->t->$i->data[$n] as $t_prom) {
                                        $tpr .= $t_prom->value;
                                    }
                                    $division .= $tpr;
                                } else {
                                    $errors[] = "СО № $so->number, напрямок № $direction->number: не створено циклограмму.";
                                }

                            }
                            $zero = count(explode(',', $so->pcu)) * 16 - strlen($division);

                            for ($f = 0; $f < $zero; $f++) {
                                $division .= '0';
                            }
                            $division = str_pad((base_convert($division, 2, 16)), 4 * $pcu_qty, '0', STR_PAD_LEFT);

                            $division .= ':';

                            $t_prom_keys .= $division;
                        }

                        $t_prom_keys = rtrim($t_prom_keys, ":");
                        $t_prom_keys .= ';';
                        $str .= $t_prom_keys;


                    } else {
                        $errors[] = "СО № $so->number, напрямок № $dir_num, фаза № $i: не встановлено t пром.";
                    }
                }
            } else {

                $errors[] = "СО № $so->number: не створено циклограмму.";
                return [$rows, $errors];
            }

            $so_row[] = $str;

            //5,6,7 пункт
            $r = '';

            for ($j = 0; $j < 16; $j++) {
                $r .= '0';
            }
            $r = str_pad((base_convert($r, 2, 16)), 4 * $pcu_qty, '0', STR_PAD_LEFT);

            $so_row[] = $r;
            $so_row[] = $r;
            $so_row[] = $r;

            // 8 пункт
            $dur = '';
            foreach ($scens->sortBy('sort') as $scen) {
                $cyclo = '';

                foreach ($so->cyclogram as $cyc) {
                    if ($cyc->scen_id === $scen->id) {
                        $cyclo = $cyc;
                    }
                }
                $data_cyclogram = json_decode($cyclo->data);
                $tosn_dur = '';
                for ($i_ = 1; $i_ <= $cyclo->phase; $i_++) {

                    $dir = $so->directions->sortBy('number')->first();
                    $dir_id = $dir->id;

                    if (isset($data_cyclogram->$dir_id->ph->$i_->time)) {

                        $tosn_dur .= $data_cyclogram->$dir_id->ph->$i_->time . ':';
                    } else {
                        $errors[] = "СО № $so->number, сценарій № $scen->sort, фаза № $i_: не встановлено t осн.";
                    }

                }

                $tosn_dur = rtrim($tosn_dur, ":");
                $dur .= $tosn_dur . ';';

            }
            $qty = explode(';', $dur);
            $y = 11 - count($qty);
            for ($m = 0; $m <= $y; $m++) {
                $dur .= 'Y;';
            }
            $dur .= 'S;R;G;Y;Y;';
            $so_row[] = $dur;


            // 9 пункт

            //проходим по сценариям и записываем дельты для даного со
            $delta = '';
            $count = 0;
            foreach ($scens->sortBy('sort') as $scen) {
                foreach ($scen->delta as $d) {
                    if ($d->so_id == $so->id && $d->delta != "" && $d->delta != null) {
                        $delta .= $d->delta . ';';
                        $count++;
                    }
                }
            }
            $zero = 16 - $count;
            for ($x = 0; $x < $zero; $x++) {
                $delta .= '0;';
            }
            $so_row[] = $delta;

            $rows[] = $so_row;
        }
        return [$rows, $errors];
    }

    public function define_key($color, $direction)
    {
        if ($direction->type === 1) {
            if ($color === 'green') {
                $key = '001';
            } elseif ($color === 'red') {
                $key = '100';
            } else {
                $key = '010';
            }
        } elseif ($direction->type === 2) {
            if ($color === 'green') {
                $key = '01';
            } elseif ($color === 'red') {
                $key = '10';
            }
        } else {
            $key = '1';
        }

        return $key;
    }

    public function prg_data($programs)
    {

        $programs = $programs->sortByDesc('type');

        //type 1 - ed
        //type 2 - wk
        //type 3 - ot

        $rows = [];
        $today = Carbon::now()->format('Y-m-d');
        foreach ($programs as $program) {
            $row = [];
            if (!empty(json_decode($program->schedule))) {

                if ($program->type === 1 || $program->type === 2 || ($program->type === 3 && $program->date >= $today)) {

                    if ($program->type === 1) {
                        $name = 'ed';
                        $date = '00.00.0000';
                    } else if ($program->type === 2) {
                        $name = 'wk';
                        $date = '00.00.0000';
                    } else if ($program->type === 3) {
                        $name = 'ot';
                        $date = $program->date;
                    }

                    $row[] = $name;
                    $row[] = $date;

                    $schedule = json_decode($program->schedule);
                    foreach ($schedule as $key => $item) {
                        if ($key == 0) {
                            if ($schedule[0]->time[0] !== '00:00') {
                                $row[] = '00:00:00,11';
                                $row[] = $item->time[0] . ":00,$item->scen_id";
                            } else {
                                $row[] = $item->time[0] . ":00,$item->scen_id";
                            }
                        }

                        if ($key > 0) {
                            //проверяем есть ли промежутки во времени и заполняем их желтым
                            if ($schedule[$key]->time[0] !== $schedule[$key - 1]->time[1]) {
                                $row[] = $schedule[$key - 1]->time[1] . ":00,11";
                                $row[] = $item->time[0] . ":00,$item->scen_id";
                            } else {
                                $row[] = $item->time[0] . ":00,$item->scen_id";
                            }
                        }

                        if ($key === count($schedule) - 1) {
                            if ($item->time[1] !== '23:59') {
                                $row[] = $item->time[1] . ":00,11";
                            }
                            $row[] = '23:59:59';
                        }
                    }

                }
            }
            $rows[] = $row;

        }
        return $rows;
    }

    public function write_prg($rows, $dk_num)
    {
        if (!is_dir(storage_path("/files/$dk_num"))) {
            mkdir(storage_path("/files/$dk_num"), 0755, true);
        }

        $fp = fopen(storage_path("/files/$dk_num/$dk_num.prg"), 'w');

        foreach ($rows as $row) {

            $str = implode('|', $row);
            fputs($fp, $str . "\n");
        }

        fclose($fp);

        return 1;
    }

    public function write_scn($rows, $dk_num)
    {
        if (!is_dir(storage_path("/files/$dk_num"))) {
            mkdir(storage_path("/files/$dk_num"), 0755, true);
        }

        $fp = fopen(storage_path("/files/$dk_num/$dk_num.scn"), 'w');

        foreach ($rows as $row) {

            $str = implode('|', $row);

            fputs($fp, $str . "\n");
        }

        fclose($fp);

        return 1;
    }

    public function write_key($rows, $dk_num)
    {
        if (!is_dir(storage_path("/files/$dk_num"))) {
            mkdir(storage_path("/files/$dk_num"), 0755, true);
        }
        $fp = fopen(storage_path("/files/$dk_num/$dk_num.key"), 'w');

        foreach ($rows as $row) {

            foreach ($row as $pcu) {
                $str = $pcu[0] . '|';
                $str .= implode(';', $pcu[1]);
                fputs($fp, $str . "\n");
            }
        }

        fclose($fp);

        return 1;
    }

    public function create_md5($dk_num)
    {
        $fp = fopen(storage_path("/files/$dk_num/$dk_num.md5"), 'w');

        fputs($fp, $dk_num . '.prg=' . md5(file_get_contents(storage_path('/files/' . $dk_num . '/' . $dk_num . '.prg'))) . "\n");
        fputs($fp, $dk_num . '.scn=' . md5(file_get_contents(storage_path('/files/' . $dk_num . '/' . $dk_num . '.scn'))) . "\n");
        fputs($fp, $dk_num . '.key=' . md5(file_get_contents(storage_path('/files/' . $dk_num . '/' . $dk_num . '.key'))) . "\n");
        fputs($fp, 'main.cfg=' . md5(file_get_contents(storage_path('/files/' . $dk_num . '/main.cfg'))) . "\n");
        fclose($fp);
        return 1;
    }

    public function write_main_cfg($param)
    {
        $num = $param->dk->number;

        if (!is_dir(storage_path("/files/$num"))) {
            mkdir(storage_path("/files/$num"), 0755, true);
        }

        $fp = fopen(storage_path("/files/$num/main.cfg"), 'w');

        foreach ($param->getAttributes() as $k => $v) {

            if ($k == 'dk_id') {
                $k = 'numDK';
                $v = $num;
            }

            if ($k == 'isKSUDDavailable') {
                if ($v === '1') {
                    $v = 'yes';
                } else {
                    $v = 'no';
                }
            }

            if ($k == 'isModemOn') {
                if ($v === '1') {
                    $v = 'on';
                } else {
                    $v = 'off';
                }
            }
            if ($k !== 'id' && $k !== 'created_at' && $k !== 'updated_at' && $k !== 'configDate') {
                fputs($fp, "$k=$v" . "\n");
            }

        }
        fputs($fp, "configDate=" . date("d.m.Y"));
        fclose($fp);

        return 1;
    }

}
