<?php

namespace App\Console\Commands;

use App\Domain\Factory\UserFactory;
use Illuminate\Console\Command;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create_user {name} {password} {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(UserFactory $factory)
    {
        $factory->create([
            'name'=>$this->argument('name'),
            'password'=>$this->argument('password'),
            'email'=>$this->argument('email'),
        ]);

        dump('login: ' . $this->argument('email'), 'password: ' . $this->argument('password'));

    }
}
