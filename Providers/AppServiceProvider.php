<?php

namespace App\Providers;

use App\Domain\Repository\ConflictRepositoryInterface;
use App\Domain\Repository\CurrentRepositoryInterface;
use App\Domain\Repository\CyclogramRepositoryInterface;
use App\Domain\Repository\DeltaSORepositoryInterface;
use App\Domain\Repository\DirectionsRepositoryInterface;
use App\Domain\Repository\DKRepositoryInterface;
use App\Domain\Repository\HistoryDKRepositoryInterface;
use App\Domain\Repository\InfoSORepositoryInterface;
use App\Domain\Repository\ParamDKRepositoryInterface;
use App\Domain\Repository\ProgramRepositoryInterface;
use App\Domain\Repository\ScenDKRepositoryInterface;
use App\Domain\Repository\SORepositoryInterface;
use App\Domain\Repository\StateDKRepositoryInterface;
use App\Persistance\Repository\ConflictRepository;
use App\Persistance\Repository\CurrentRepository;
use App\Persistance\Repository\CyclogramRepository;
use App\Persistance\Repository\DeltaSORepository;
use App\Persistance\Repository\DirectionsRepository;
use App\Persistance\Repository\DKRepository;
use App\Persistance\Repository\HistoryDKRepository;
use App\Persistance\Repository\InfoSORepository;
use App\Persistance\Repository\ParamDKRepository;
use App\Persistance\Repository\ProgramRepository;
use App\Persistance\Repository\ScenDKRepository;
use App\Persistance\Repository\SORepository;
use App\Persistance\Repository\StateDKRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            DKRepositoryInterface::class,
            function () {
                return new DKRepository();
            }
        );

        $this->app->bind(
            SORepositoryInterface::class,
            function () {
                return new SORepository();
            }
        );

        $this->app->bind(
            ScenDKRepositoryInterface::class,
            function () {
                return new ScenDKRepository();
            }
        );

        $this->app->bind(
            DirectionsRepositoryInterface::class,
            function () {
                return new DirectionsRepository();
            }
        );

        $this->app->bind(
            ProgramRepositoryInterface::class,
            function () {
                return new ProgramRepository();
            }
        );

        $this->app->bind(
            ConflictRepositoryInterface::class,
            function () {
                return new ConflictRepository();
            }
        );

        $this->app->bind(
            CyclogramRepositoryInterface::class,
            function () {
                return new CyclogramRepository();
            }
        );
        $this->app->bind(
            ParamDKRepositoryInterface::class,
            function () {
                return new ParamDKRepository();
            }
        );
        $this->app->bind(
            DeltaSORepositoryInterface::class,
            function () {
                return new DeltaSORepository();
            }
        );

        $this->app->bind(
            HistoryDKRepositoryInterface::class,
            function () {
                return new HistoryDKRepository();
            }
        );
        $this->app->bind(
            StateDKRepositoryInterface::class,
            function () {
                return new StateDKRepository();
            }
        );
        $this->app->bind(
            InfoSORepositoryInterface::class,
            function () {
                return new InfoSORepository();
            }
        );
        $this->app->bind(
            CurrentRepositoryInterface::class,
            function () {
                return new CurrentRepository();
            }
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
