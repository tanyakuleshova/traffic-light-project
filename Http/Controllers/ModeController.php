<?php

namespace App\Http\Controllers;

use App\Domain\Repository\DKRepositoryInterface;
use App\Domain\Repository\SORepositoryInterface;
use Illuminate\Http\Request;

class ModeController extends Controller
{
    protected $dk_repository;
    protected $so_repository;

    public function __construct(DKRepositoryInterface $dk_repository, SORepositoryInterface $so_repository)
    {
        $this->dk_repository = $dk_repository;
        $this->so_repository = $so_repository;
    }

    public function get_so_mode(Request $request)
    {
        $so = $this->so_repository->getById($request->get('so_id'));

        return response(['dk_num' => $so->dk->number]);
    }

    public function get_dk_mode(Request $request)
    {
        $dk = $this->dk_repository->getById($request->get('dk_id'));

        $content = view('mode_dk', compact('dk'))->render();
        return response(['content' => $content]);
    }
}
