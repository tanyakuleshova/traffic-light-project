<?php

namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;

trait Creatable
{
    public function add(Request $request)
    {
        $validated = $this->validation->prepare($request->get('data'));

        if (empty($validated['errors'])) {
            return $this->factory->create($validated['data']);
        }

        return response(['errors' => $validated['errors']]);

    }
}
