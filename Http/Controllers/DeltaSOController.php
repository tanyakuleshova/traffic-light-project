<?php

namespace App\Http\Controllers;

use App\Domain\Factory\DeltaSOFactory;
use App\Domain\Repository\DeltaSORepositoryInterface;
use App\Domain\Repository\DKRepositoryInterface;
use App\Domain\Service\DeltaSOService;
use App\Services\Files\DKFiles;
use App\Services\Validation\DeltaSODataValidation;
use Illuminate\Http\Request;

class DeltaSOController extends Controller
{
    protected $dk_repository;

    protected $repository;
    protected $factory;
    protected $validation;
    protected $delta_service;
    protected $dk_files_service;


    public function __construct(
        DKRepositoryInterface      $dk_repository,

        DeltaSORepositoryInterface $repository,
        DeltaSOFactory             $recordFactory,
        DeltaSODataValidation      $validation,
        DeltaSOService             $delta_service,
        DKFiles                    $dk_files_service)
    {
        $this->dk_repository = $dk_repository;

        $this->repository = $repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
        $this->delta_service = $delta_service;
        $this->dk_files_service = $dk_files_service;
    }


    public function get(Request $request)
    {
        $dk = $this->dk_repository->getById((int)$request->get('id'));
        $so = (count($dk->so)) ? $dk->so->sortBy('number') : [];
        $content = view('forms.delta_so', compact('so', 'dk'))->render();

        $this->dk_files_service->generate($dk);

        return response(['dk' => $dk, 'so' => $so, 'content' => $content, 'success' => (bool)count($dk->so)]);
    }

    public function save(Request $request)
    {
        $this->delta_service->process($request->get('data'), $request->get('deleted'));

        return $this->get($request);
    }

}
