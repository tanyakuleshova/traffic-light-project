<?php

namespace App\Http\Controllers;

use App\Domain\Factory\ConflictFactory;
use App\Domain\Repository\ConflictRepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Validation\ConflictDataValidation;
use Illuminate\Http\Request;

class ConflictController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;

    public function __construct(
        ConflictRepositoryInterface $repository,
        ConflictFactory $recordFactory,
        ConflictDataValidation $validation)
    {
        $this->repository = $repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
    }

    public function get(Request $request)
    {
        $conflict = $this->repository->getByField(['so_id', (int)$request->get('so_id')]);

        return response(['success'=>$conflict->isNotEmpty(), 'conflict' => $conflict]);
    }

}
