<?php

namespace App\Http\Controllers;

use App\Domain\Factory\ProgramFactory;
use App\Domain\Repository\ProgramRepositoryInterface;
use App\Domain\Repository\ScenDKRepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Files\DKFiles;
use App\Services\Validation\ProgramDataValidation;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;

    protected $scen_repository;
    protected $dk_file;

    public function __construct(
        ProgramRepositoryInterface $repository,
        ScenDKRepositoryInterface $scen_repository,
        ProgramFactory $recordFactory,
        ProgramDataValidation $validation,
        DKFiles $dk_file
    )
    {
        $this->repository = $repository;
        $this->scen_repository = $scen_repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
        $this->dk_file = $dk_file;
    }

    public function program_data(Request $request)
    {
        $scenarios = $this->scen_repository->getByField(['dk_id', (int)$request->get('dk_id')]);
        $programs = $this->repository->getByField(['so_id', (int)$request->get('so_id')]);

        return response(['scenarios'=>$scenarios, 'programs'=>$programs]);
    }

    public function save(Request $request){
        $program = $this->add($request);
        $all = $this->repository->getByField(['so_id', $program->so_id]);

        $this->dk_file->generate($program->so->dk);
        return response(['all'=>$all, 'program'=>$program]);
    }
}
