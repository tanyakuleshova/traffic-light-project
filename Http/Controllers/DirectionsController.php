<?php

namespace App\Http\Controllers;

use App\Domain\Factory\DirectionsFactory;
use App\Domain\Repository\DirectionsRepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Files\DKFiles;
use App\Services\Validation\DirectionsDataValidation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class DirectionsController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;
    protected $dk_file;

    public function __construct(
        DirectionsRepositoryInterface $repository,
        DirectionsFactory             $recordFactory,
        DirectionsDataValidation      $validation,
        DKFiles                       $dk_file)
    {
        $this->repository = $repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
        $this->dk_file = $dk_file;
    }

    public function delete(Request $request)
    {
        return $this->repository->delete((int)$request->get('id'));
    }

    public function save(Request $request)
    {
        $direction = $this->add($request);

        if ($direction instanceof Response) {
            return $direction;
        }
        $this->dk_file->generate($direction->so->dk);
        return $direction;

    }

}
