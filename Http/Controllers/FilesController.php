<?php

namespace App\Http\Controllers;

use App\Domain\Repository\DKRepositoryInterface;
use App\Services\Files\DKFiles;
use Illuminate\Http\Request;

class FilesController extends Controller
{
    protected $service;
    protected $dk_repository;

    public function __construct(DKFiles $service, DKRepositoryInterface $dk_repository)
    {
        $this->service = $service;
        $this->dk_repository = $dk_repository;
    }

    public function dk_data(Request $request){

        $dk_id = (int)$request->get('id');
        $dk = $this->dk_repository->getById($dk_id);

        return $this->service->generate($dk);
    }

    public function download_file($dk_num, $format): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        if ($format === 'cfg') {
            return response()->download(storage_path("files/$dk_num/main.$format"));
        }
        return response()->download(storage_path("files/$dk_num/$dk_num.$format"));
    }

}
