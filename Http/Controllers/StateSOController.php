<?php

namespace App\Http\Controllers;

use App\Domain\Repository\InfoSORepositoryInterface;
use App\Domain\Repository\SORepositoryInterface;
use App\Services\Translate\TranslateStateDK;
use Illuminate\Http\Request;

class StateSOController extends Controller
{
    protected $info_so_repository;
    protected $so_repository;

    public function __construct(InfoSORepositoryInterface $info_so_repository, SORepositoryInterface $so_repository)
    {
        $this->info_so_repository = $info_so_repository;
        $this->so_repository = $so_repository;
    }

    public function get(Request $request)
    {
        $content = 'Немає данних.';

        $so = $this->so_repository->getById((int)$request->get('so_id'));

        $state = $this->info_so_repository->getByParams([['numDK', $so->dk->number], ['numSO', $so->number]]);

        if($state->isNotEmpty()){
            $state = $state->first();
            $pcu = explode(',', $so->pcu);

            $rows = [];
            foreach ($so->directions as $direction){
                $keys = explode(',',$direction->keys);
                $limit = explode(',',$direction->limit);
                $load = explode(',',$direction->load);
                $emitters = explode(',',$direction->emitters_qty);

                if(count($keys) === 3){
                    $type = 'ТН';
                }elseif (count($keys) === 2){
                    $type = 'ПН';
                }else{
                    $type = 'ЗС';
                }

                foreach ($keys as $k=>$key){

                    //делим номер ключа на 8 (столько в каждом бск) и получаем номер бск по счету
                    //из массива бск данного со берем по счету нужный бск

                    $pcu_num = ceil($key/8);
                    $pcu_num = $pcu[(int)$pcu_num-1];

                    $row = [];
                    $row[] = $pcu_num; // номер pcu
                    $row[] = $key; // номер канала
                    $row[] = $type; // тип направления

                    // цвет лампочки
                    if($k == 0 && $type == 'ТН'){
                        $row[] = '#e85d5f'; //red
                    }elseif($k ==0 && $type == 'ЗС'){
                        $row[] = '#71dcc5   '; //green
                    } elseif($k ==1 && $type == 'ТН'){
                        $row[] = '#facf5a'; //yel
                    }elseif($k ==1 && $type == 'ПН'){
                        $row[] = '#71dcc5';//green
                    }else{
                        $row[] = '#71dcc5';//green
                    }

                    $row[] = TranslateStateDK::translate($state->stateDK->connState); // диагностика канала

                    // I, mA

                    $ma = $this->getI($pcu_num, $k, $so->dk);
                    $row[] = $ma;

                    // U, V
                    $v = $state->stateDK->dkVoltIn;
                    $row[] = $v;

                    $row[] =  ($ma !== "") ? $ma*(int)$v : "" ;// P, W

                    $row[] = $emitters[$k]; // количество ламп
                    $row[] = $limit[$k]; // порог контроля
                    $row[] = $load[$k]; // общая нагрузка

                    $rows[] = $row;
                }
            }
//            dd($rows);

            $content = view('state_so', compact('rows'))->render();
        }

        return response(['content'=>$content, 'dk_num'=>$so->dk->number]);
    }

    function getI($i,$key,$dk){

        $cur_collection = $dk->current->sortByDesc('updateDKtime');

        foreach ($cur_collection as $current){

            $pcu = 'currentBSK'.$i;
            $ma = explode(',',$current->$pcu);

            if(is_array($ma)){
                if(isset($ma[$key])){
                    if($ma[$key] < 65000){
                        return $ma[$key];
                    }
                }
            }
        }
        return "";
    }

}
