<?php

namespace App\Http\Controllers;

use App\Domain\Repository\StateDKRepositoryInterface;
use Illuminate\Http\Request;

class StateDKController extends Controller
{
    protected $repository;

    public function __construct(StateDKRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function get(Request $request)
    {
        $content = 'Немає данних.';

        $state = $this->repository->getByParams([['numDK', $request->get('dk_num')]]);


        if($state->isNotEmpty()){
            $state = $state->first();

            $content = view('state_dk', compact('state'))->render();
        }

        return response(['content'=>$content]);
    }

}
