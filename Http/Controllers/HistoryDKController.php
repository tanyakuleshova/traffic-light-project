<?php

namespace App\Http\Controllers;

use App\Domain\Repository\HistoryDKRepositoryInterface;
use Illuminate\Http\Request;

class HistoryDKController extends Controller
{
    protected $repository;

    public function __construct(HistoryDKRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function get(Request $request)
    {
        $start = '';
        $end = '';
        $content = 'Немає данних.';

        if(!$request->get('start') || !$request->get('end')){
            $history = $this->repository->getByParams([['numDK', $request->get('dk_number')]], 100);

        }else{

            $params = [
                ['numDK', $request->get('dk_number')],
                ['updateDKtime', '>=', $request->get('start')],
                ['updateDKtime', '<=', $request->get('end')]
            ];

            $history = $this->repository->getByParams($params);
        }

        if(!empty($history)){
            $end = $history->first()->updateDKtime;
            $start_collection = $this->repository->getByParamsLast([['numDK', $request->get('dk_number')]], 100);
            $start = $start_collection->last()->updateDKtime;
            $content = view('history_dk', compact('history'))->render();
        }

        return response(['content'=>$content, 'start'=>$start, 'end'=>$end]);

    }

}
