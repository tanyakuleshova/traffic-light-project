<?php

namespace App\Http\Controllers;

use App\Domain\Service\MapService;
use Illuminate\Http\Request;

class MapController extends Controller
{
    protected $service;

    public function __construct(MapService $mapService)
    {
        $this->service = $mapService;
    }

    public function map(Request $request)
    {
        $prefix = $request->route()->getPrefix();
        $tl_data = $this->service->tl_data();
        return view($prefix . '.map', compact('tl_data'));
    }

    public function delete_tl(Request $request)
    {
        return $this->service->delete_TL((int)$request->get('id'), $request->get('type'));
    }
}
