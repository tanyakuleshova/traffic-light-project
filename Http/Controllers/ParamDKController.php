<?php

namespace App\Http\Controllers;

use App\Domain\Factory\ParamDKFactory;
use App\Domain\Repository\ParamDKRepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Files\DKFiles;
use App\Services\Validation\ParamDKDataValidation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ParamDKController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;
    protected $dk_file;

    public function __construct(

        ParamDKRepositoryInterface $repository,
        ParamDKFactory             $factory,
        ParamDKDataValidation      $validation,
        DKFiles                    $dk_file
    )
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->validation = $validation;
        $this->dk_file = $dk_file;
    }

    public function save(Request $request)
    {
        //todo: валидация полей
        $param_dk = $this->add($request);

        $this->dk_file->generate($param_dk->dk);

        if ($param_dk instanceof Response) {
            return $param_dk;
        }
        return $param_dk;
    }
}
