<?php

namespace App\Http\Controllers;

use App\Domain\Factory\CyclogramFactory;
use App\Domain\Repository\CyclogramRepositoryInterface;
use App\Domain\Repository\SORepositoryInterface;
use App\Domain\Service\DeltaSOService;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Files\DKFiles;
use App\Services\Validation\CyclogramDataValidation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CyclogramController extends Controller
{
    protected $repository;
    protected $factory;
    protected $validation;

    protected $so_repository;
    protected $delta_service;
    protected $dk_files_service;

    public function __construct(
        CyclogramRepositoryInterface $repository,
        CyclogramFactory $factory,
        CyclogramDataValidation $validation,

        SORepositoryInterface $so_repository,
        DeltaSOService $delta_service,
        DKFiles $dk_files_service
    )
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->validation = $validation;

        $this->so_repository = $so_repository;
        $this->delta_service = $delta_service;
        $this->dk_files_service = $dk_files_service;

    }

    public function delete(Request $request)
    {
        return $this->repository->delete((int)$request->get('id'));
    }

    public function save(Request $request)
    {
        $this->delta_service->process($request->get('create_delta'), $request->get('delete_delta'));

        foreach($request->get('data') as $item){
            $validated = $this->validation->prepare($item);

            if (empty($validated['errors'])) {
                $this->factory->create($validated['data']);
            }else{
                return response(['errors' => $validated['errors']]);
            }
        }
        $so = $this->so_repository->getById($request->get('so_id'));

        $this->dk_files_service->generate($so->dk);

        return response(['so'=>$so]);
    }

}
