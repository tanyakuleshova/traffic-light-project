<?php

namespace App\Http\Controllers;

use App\Domain\Factory\ScenDKFactory;
use App\Domain\Repository\ScenDKRepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Files\DKFiles;
use App\Services\Validation\ScenDKDataValidation;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ScenDKController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;
    protected $dk_file;

    public function __construct(
        ScenDKRepositoryInterface $repository,
        ScenDKFactory             $recordFactory,
        ScenDKDataValidation      $validation,
        DKFiles                   $dk_file)
    {
        $this->repository = $repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
        $this->dk_file = $dk_file;
    }

    public function get(Request $request)
    {
        $scens = $this->repository->getByField(['dk_id', (int)$request->get('id')]);
        return response(['data' => $scens, 'success' => $scens->isNotEmpty()]);
    }

    public function delete(Request $request)
    {
        return $this->repository->delete((int)$request->get('id'));
    }

    public function save(Request $request)
    {
        $scen = $this->add($request);

        if ($scen instanceof Response) {
            return $scen;
        }
        $this->dk_file->generate($scen->dk);
        return $scen;

    }

}
