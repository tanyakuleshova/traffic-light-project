<?php

namespace App\Http\Controllers;

use App\Domain\Factory\SOFactory;
use App\Domain\Repository\SORepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Validation\SODataValidation;
use Illuminate\Http\Request;

class SOController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;

    public function __construct(
        SORepositoryInterface $repository,
        SOFactory $recordFactory,
        SODataValidation $validation)
    {
        $this->repository = $repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
    }

    public function get(Request $request)
    {
        $so = $this->repository->getById((int)$request->get('id'));
        return response(['so'=>$so, 'success'=>(bool)$so]);
    }

    public function so_pcu(Request $request)
    {
        $so = $this->repository->getById((int)$request->get('id'));
        $pcu = '';
        foreach ($so->dk->so as $item){
            $pcu .= $item->pcu . ',';
        }
        $pcu = rtrim($pcu, ",");

        return response(['so'=>$so, 'pcu'=>$pcu]);
    }
}
