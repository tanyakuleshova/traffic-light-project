<?php

namespace App\Http\Controllers;

use App\Domain\Factory\DKFactory;
use App\Domain\Repository\DKRepositoryInterface;
use App\Http\Controllers\Traits\Creatable;
use App\Services\Validation\DKDataValidation;
use Illuminate\Http\Request;

class DKController extends Controller
{
    use Creatable;

    protected $repository;
    protected $factory;
    protected $validation;

    public function __construct(
        DKRepositoryInterface $repository,
        DKFactory $factory,
        DKDataValidation $validation)
    {
        $this->repository = $repository;
        $this->factory = $factory;
        $this->validation = $validation;
    }

    public function dk_list(Request $request)
    {
        $content = view('forms.so_num_ch')->render();
        $dk_list = $this->repository->getByNumber($request->get('data'));
        return response(['data' => $dk_list, 'content' => $content]);
    }
}
