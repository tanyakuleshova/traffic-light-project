<?php

namespace App\Domain\Service;

use App\Domain\Repository\DKRepositoryInterface;
use App\Domain\Repository\SORepositoryInterface;
use Illuminate\Database\QueryException;

class MapService
{
    protected $dk_repository;
    protected $so_repository;

    public function __construct(DKRepositoryInterface $dk_repository, SORepositoryInterface $so_repository)
    {
        $this->dk_repository = $dk_repository;
        $this->so_repository = $so_repository;
    }

    public function tl_data()
    {
        $data = [];
        $data['dk'] = $this->dk_repository->getAll();
        $data['so'] = $this->so_repository->getAll();
        return $data;
    }

    public function delete_TL($id, $type)
    {
        if($type == 'dk'){
            $repository = $this->dk_repository;
        }else{
            $repository = $this->so_repository;
        }

        try{
            $repository->delete($id);
            return response(['success' => true]);
        }catch (QueryException $e){
            $so = [];

            if($e->getCode() == '23000'){
                $so = $this->so_repository->getByDK($id);
            }

            return response(['success' => false, 'code'=>$e->getMessage(), 'so' => $so]);
        }

    }
}
