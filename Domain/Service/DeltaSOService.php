<?php

namespace App\Domain\Service;

use App\Domain\Factory\DeltaSOFactory;
use App\Domain\Repository\DeltaSORepositoryInterface;
use App\Services\Validation\DeltaSODataValidation;

class DeltaSOService
{
    protected $repository;
    protected $factory;
    protected $validation;

    public function __construct(
        DeltaSORepositoryInterface $repository,
        DeltaSOFactory             $recordFactory,
        DeltaSODataValidation      $validation)
    {
        $this->repository = $repository;
        $this->factory = $recordFactory;
        $this->validation = $validation;
    }

    public function process($data, $deleted)
    {
        foreach ($data as $item){

            $validated = $this->validation->prepare($item);

            if (empty($validated['errors'])) {
                $this->factory->create($validated['data']);
            }
        }

        if(count($deleted)){
            $this->repository->deleteIn($deleted);
        }
    }

}
