<?php

namespace App\Domain\Repository;

interface RepositoryInterface
{
    public function getAll();
    public function getById($id);
    public function delete($id);
}
