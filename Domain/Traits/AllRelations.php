<?php

namespace App\Domain\Traits;

trait AllRelations
{
    public function scopeWithAll($query)
    {
        if($this->relations){
            $query->with($this->relations);
        }
    }
}
