<?php

namespace App\Domain\Traits;

use Illuminate\Support\Facades\Auth;

trait Userable
{
    public function save(array $options = array())
    {
        $this->user_id = Auth::id();
        parent::save($options);
    }
}