<?php

namespace App\Domain\Traits;


trait Creatable
{
    public function create($fields)
    {
        $class = mb_substr(__CLASS__, 0, -7);
        $factory = str_replace("Factory","Models", $class);

        if(isset($fields['id'])){
            $model = $factory::find($fields['id']);
        }else{
            $model = new $factory();
        }

        foreach ($fields as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;

    }
}
