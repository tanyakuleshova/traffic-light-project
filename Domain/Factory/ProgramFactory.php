<?php

namespace App\Domain\Factory;

use App\Domain\Traits\Creatable;

class ProgramFactory
{
    public function create($fields)
    {
        $class = mb_substr(__CLASS__, 0, -7);
        $factory = str_replace("Factory","Models", $class);

        if(isset($fields['id'])){
            if(isset($fields['date'])){
                $model = $factory::where('id',$fields['id'])->where('date', $fields['date'])->first();
            }else{
                $model = $factory::where('id',$fields['id'])->first();
            }
        }else{
            $model = new $factory();
        }

        foreach ($fields as $key => $value) {
            $model->$key = $value;
        }

        $model->save();

        return $model;

    }

}
