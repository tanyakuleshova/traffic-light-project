<?php

namespace App\Domain\Factory;

use App\Domain\Traits\Creatable;

class SOFactory
{
    use Creatable;

    protected $relation = 'dk';
}
