<?php

namespace App\Domain\Models;

use App\Domain\Traits\AllRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Directions extends Model
{
    use HasFactory;

    protected $table = 'directions';
    protected $guarded = ['id'];

    public function so()
    {
        return $this->hasOne(SO::class, 'id', 'so_id');
    }
}
