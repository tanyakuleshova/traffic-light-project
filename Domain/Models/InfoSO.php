<?php

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InfoSO extends Model
{
    use HasFactory;

    protected $table = 'infoSO';
    protected $guarded = ['id'];

    public function stateDK()
    {
        return $this->hasOne(StateDK::class, 'numDK', 'numDK');
    }

}
