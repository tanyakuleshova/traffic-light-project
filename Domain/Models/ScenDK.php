<?php

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScenDK extends Model
{
    use HasFactory;

    protected $table = 'scen_dk';
    protected $guarded = ['id'];

    public function dk()
    {
        return $this->hasOne(DK::class, 'id', 'dk_id');
    }

    public function cyclogram()
    {
        return $this->hasMany(Cyclogram::class, 'scen_id', 'id');
    }

    public function delta()
    {
        return $this->hasMany(DeltaSO::class, 'scen_id', 'id');
    }

}
