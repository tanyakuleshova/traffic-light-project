<?php

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StateDK extends Model
{
    use HasFactory;

    protected $table = 'stateDK_new';
    protected $guarded = ['id'];

    public function dk()
    {
        return $this->hasMany(DK::class, 'number', 'numDK');
    }

}
