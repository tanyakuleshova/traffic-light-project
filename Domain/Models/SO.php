<?php

namespace App\Domain\Models;

use App\Domain\Traits\AllRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SO extends Model
{
    use HasFactory;

    protected $table = 'so';
    protected $guarded = ['id'];

    public function dk()
    {
        return $this->hasOne(DK::class, 'id', 'dk_id');
    }

    public function directions()
    {
        return $this->hasMany(Directions::class, 'so_id', 'id')->orderBy('number');
    }
    public function conflict()
    {
        return $this->hasOne(Conflict::class, 'so_id', 'id');
    }
    public function cyclogram()
    {
        return $this->hasMany(Cyclogram::class, 'so_id', 'id');
    }

    public function program()
    {
        return $this->hasMany(Program::class, 'so_id', 'id');
    }

    public function delta()
    {
        return $this->hasMany(DeltaSO::class, 'so_id', 'id');
    }

    public function getDelta($scen_id, $type)
    {
        foreach ($this->delta as $delta){
            if($delta->scen_id == $scen_id){

                if($type === 'id'){
                    return $delta->id;
                }elseif($type === 'value'){
                    return $delta->delta;
                }

            }
        }
        return "";
    }
}
