<?php

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryDK extends Model
{
    use HasFactory;

    protected $table = 'stateDK_hist';
    protected $guarded = ['id'];

    public function dk()
    {
        return $this->hasMany(DK::class, 'number', 'numDK');
    }

}
