<?php

namespace App\Domain\Models;

use App\Domain\Traits\AllRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Conflict extends Model
{
    use HasFactory;

    protected $table = 'conflicts';
    protected $guarded = ['id'];

}
