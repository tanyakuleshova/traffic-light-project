<?php

namespace App\Domain\Models;

use App\Domain\Traits\AllRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParamDK extends Model
{
    use HasFactory;

    protected $table = 'param_dk';
    protected $guarded = ['id'];

    public function dk()
    {
        return $this->hasOne(DK::class, 'id', 'dk_id');
    }
    public static function boot()
    {
        parent::boot();
        static::saving(function ($model) {
            if(!is_string($model->isModemOn)){
                $model->isModemOn = 0;
            }

            if(!is_string($model->isKSUDDavailable)){
                $model->isKSUDDavailable = 0;
            }
        });
    }


}
