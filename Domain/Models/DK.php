<?php

namespace App\Domain\Models;

use App\Domain\Traits\AllRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DK extends Model
{
    use HasFactory;

    protected $table = 'dk';
    protected $guarded = ['id'];

    public function so()
    {
        return $this->hasMany(SO::class, 'dk_id', 'id')
            ->with('cyclogram', 'program');
    }

    public function scen()
    {
        return $this->hasMany(ScenDK::class, 'dk_id', 'id');
    }

    public function param()
    {
        return $this->hasOne(ParamDK::class, 'dk_id', 'id');
    }
    public function current()
    {
        return $this->hasMany(Current::class, 'numDK', 'number');
    }

}
