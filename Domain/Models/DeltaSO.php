<?php

namespace App\Domain\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DeltaSO extends Model
{
    use HasFactory;

    protected $table = 'delta_so';
    protected $guarded = ['id'];

    public function so()
    {
        return $this->belongsTo('so');
    }

    public function scen()
    {
        return $this->belongsTo('scen');
    }

}
