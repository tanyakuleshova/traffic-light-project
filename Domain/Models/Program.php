<?php

namespace App\Domain\Models;

use App\Domain\Traits\AllRelations;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    use HasFactory;

    protected $table = 'programs';
    protected $guarded = ['id'];

    public function so()
    {
        return $this->hasOne(SO::class, 'id', 'so_id');
    }

}
